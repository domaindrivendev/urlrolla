# DomainDrivenDev
## Intro
## What are we building?
### www.urlrolla.com
- sign up with a user account
- store multiple links in a group
- label that group with a name
- share that group of links with the public
- share that group only with private friends or by invite
- comment or add notes on that group or each individual link
## Where do we begin?
### Front End aka Client-side code
- React
  - Vite
  - TypeScript
  - Styled Components
  - `yarn create vite`
- Angular
- Svelte
- Vue
### Deployment
- Netlify
  - User management
### DevTools
- Visual Studio Code
- Draw.io
- Gitlab
- Terminal Mac OS